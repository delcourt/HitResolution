#pragma once
#include <iostream>
#include <vector>
#include <map>
#include <set>
#include "TTree.h"
#include "TFile.h"

struct tree_entry{
    int     detID1;
    int     detID2;
    float   pitch1;
    float   momentum; 
    float   pairPath;
    float   trackChi2;
    int     clusterW1;
    int     clusterW2;
    int     numHits;
    float   hitDX;
    float   trackDX;
    float   trackDXE;
    float   atEdge1;
    float   atEdge2;
};

class TreeReader{
    private:
    TFile * _ff;
    TTree * _tt;
    uint64_t        event_counter; 
    uint64_t        n_entries;


    public:
    
    TreeReader(std::string file_name);
    ~TreeReader();
    int      getEvent(int event_number = -1);
    int      get_event_counter();
    uint64_t getEntries();
    void    reset();
    std::string   getProgress();
    
    tree_entry event;
};


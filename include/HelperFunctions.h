#include <string>
#include <dirent.h>
#include <vector>
std::string name_list[]= {"","","","TIB","TID","TOB","TEC"};
inline int get_subdet(int detId){return (detId >> 25) & 0x7;}
inline int get_layer(int detId){return ((detId >> 14) & 0x7);}
inline int get_key(int detId){return (get_layer(detId))+(get_subdet(detId)<<8);}

inline std::string get_name(int detId){
    return name_list[get_subdet(detId)]+" Layer "+std::to_string(get_layer(detId));
}

inline std::string get_name_from_key(int key){
    return name_list[key>>8]+" Layer "+std::to_string(key&0xFF);
}


std::vector <std::string> get_file_list(std::string path){
    std::vector <std::string> f_list;
    DIR *dir;
    struct dirent *ent;
    if ((dir = opendir (path.c_str())) != NULL) {
        /* print all the files and directories within directory */
        while ((ent = readdir (dir)) != NULL) {
            f_list.push_back(ent->d_name);
        }
        closedir (dir);
    }
    return f_list;
}

#pragma once
#include <vector>
#include <string>
#include "TH1F.h"
#include "TH1I.h"
#include "TGraph.h"
#include "TEfficiency.h"
#include "TH2.h"


class EffContainer{
    public:
        EffContainer(std::string name, int n_bins, float x_0, float x_1);
        ~EffContainer();
        void Fill(float x,bool matched);
        std::pair <float,float>               GetEff();
        std::vector < std::pair <float,float> > GetCbcEff();
        std::vector < std::pair <float,float> > GetCicEff();    
        void Write();
        int get_entries();
    private:
        void ComputeRatio();
        TH1 * numerator, * denominator, * ratio;
        TEfficiency * teff;
        int n_entries_processed;
        float start,stop;
};

class EffContainer2D{
    public:
        EffContainer2D(std::string name, int n_bins_x, float x_0, float x_1,int n_bins_y, float y_0, float y_1);
        ~EffContainer2D();
        void Fill(float x,float y, bool matched);
        void Write();
    private:
        TEfficiency * teff;
};


class HistManager{
    public:
        HistManager(std::string saveFile);
        ~HistManager();
        void Write();
        TH1 *          AddTH1(std::string name, int n_bins, float x_0, float x_1);
        TGraph *       AddTGraph(std::string name, const std::vector <float> & data_x,const std::vector <float> & data_y);
        TH2 *          AddTH2(std::string name, int n_bins_x, float x_0, float x_1,int n_bins_y, float y_0, float y_1);
        TGraph *       AddTrend(std::string name, float value, float time);
        EffContainer * AddEfficiency(std::string name, int n_bins, float x_0, float x_1);
        EffContainer2D * AddEfficiency2D(std::string name, int n_bins_x, float x_0, float x_1, int n_bins_y, float y_0, float y_1);
        void           AddObject(TObject * obj);

    private:
        std::vector <TH1    *>          hists;
        std::vector <TGraph *>          graphs;
        std::vector <TH2    *>          hists_2D;
        std::vector <EffContainer *>    efficiencies;
        std::vector <EffContainer2D *>  efficiencies2D;
        std::vector <TObject *>         additional_objects;
        std::string save_file;

};

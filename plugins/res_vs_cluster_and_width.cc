#include "TreeReader.h"
#include "HelperFunctions.h"
#include "HistManager.h"
#include <iostream>
#include <set>
using namespace std;
string data_dir = "/eos/user/a/aguzel/share/TrackerDPG/2023B/";

int main(){
    auto file_list = get_file_list(data_dir);
    int file_id = 0;
    auto hm = HistManager("histograms_per_size.root");
    map <string,int> entries;
    map <string,TH1 *> hist_dd;
    map <string,TH1 *> hist_te;
    
    for (auto file_name : file_list){


        if (file_name.find(".root") == -1)
            continue;
        auto tr = TreeReader(data_dir+file_name);
        cout<<"Opened file "<< file_id<<"/"<<file_list.size()<<" : " <<file_name<<" with "<<tr.getEntries()<<" entries"<<endl;
        file_id++;


        while(tr.getEvent()){
            if (tr.event.momentum < 3 or tr.event.pairPath > 7)
                continue;
            if (tr.event.clusterW1 > 4 or tr.event.clusterW2 > 4)
                continue;
            if (tr.event.clusterW1 != tr.event.clusterW2)
                continue;
            if (tr.event.numHits < 6 || tr.event.trackChi2 < 0.001)
                continue;
            if (tr.event.atEdge1 || tr.event.atEdge2)
                continue;
            if (tr.event.trackDXE >= 0.0025)
                continue;

            string hist_name = to_string(tr.event.detID1)+"_w"+to_string(tr.event.clusterW1);
            entries[hist_name]++;

            if (entries[hist_name] == 1){
                hist_dd[hist_name] = hm.AddTH1("Double_difference_"+hist_name, 21, -0.021, 0.021);
                hist_te[hist_name] = hm.AddTH1("Track_error_"+hist_name, 21, 0, 0.021);
            }
            hist_dd[hist_name]->Fill((tr.event.hitDX-tr.event.trackDX));
            hist_te[hist_name]->Fill(tr.event.trackDXE);
        }
    }
    hm.Write();
}

#include "TreeReader.h"
#include "HelperFunctions.h"
#include "HistManager.h"
#include <iostream>
#include <set>
using namespace std;
string data_dir = "/eos/user/a/aguzel/share/TrackerDPG/2023B/";

int main(){
    auto file_list = get_file_list(data_dir);
    int file_id = 0;
    auto hm = HistManager("histograms.root");
    map <int, set <float> > thickness;
    for (auto file_name : file_list){

        if (file_name.find(".root") == -1)
            continue;
        auto tr = TreeReader(data_dir+file_name);
        cout<<"Opened file "<< file_id<<"/"<<file_list.size()<<" : " <<file_name<<" with "<<tr.getEntries()<<" entries"<<endl;
        file_id++;


        while(tr.getEvent()){
            thickness[get_key(tr.event.detID1)].insert(tr.event.pitch1);
        }
        for (auto kk:thickness){
            cout<<get_name_from_key(kk.first)<<" : ";
            for (auto th:kk.second)
                cout<<th<<" ";
            cout<<endl;
        }
        break;
    }
}

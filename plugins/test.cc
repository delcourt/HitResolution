#include "TreeReader.h"
#include "HelperFunctions.h"
#include "HistManager.h"
#include <iostream>
#include <set>
using namespace std;
string data_dir = "/eos/user/a/aguzel/share/TrackerDPG/2023B/";

int main(){
    auto file_list = get_file_list(data_dir);
    int file_id = 0;
    auto hm = HistManager("histograms.root");
    map <int,int> entries;
    map <int,TH1 *> hist_dd;
    map <int,TH1 *> hist_te;
    
    for (auto file_name : file_list){


        if (file_name.find(".root") == -1)
            continue;
        auto tr = TreeReader(data_dir+file_name);
        cout<<"Opened file "<< file_id<<"/"<<file_list.size()<<" : " <<file_name<<" with "<<tr.getEntries()<<" entries"<<endl;
        file_id++;


        while(tr.getEvent()){
            if (tr.event.momentum < 3 or tr.event.pairPath > 7)
                continue;
            if (tr.event.clusterW1 > 4 or tr.event.clusterW2 > 4)
                continue;
            if (tr.event.numHits < 6)
                continue;
            entries[tr.event.detID1]++;

            if (entries[tr.event.detID1] == 1){
                hist_dd[tr.event.detID1] = hm.AddTH1("Double_difference_"+to_string(tr.event.detID1), 21, -0.021, 0.021);
                hist_te[tr.event.detID1] = hm.AddTH1("Track_error_"+to_string(tr.event.detID1), 21, 0, 0.021);
            }
            hist_dd[tr.event.detID1]->Fill((tr.event.hitDX-tr.event.trackDX));
            hist_te[tr.event.detID1]->Fill(tr.event.trackDXE);
        }
    }
    hm.Write();
}

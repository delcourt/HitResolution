Code that was used to produce early 2023 results

Usage:
1) Compile:
```bash
mkdir bin
make
```

2) Harvest data
```bash
./bin/res_vs_cluster_and_width.out
```

3) Get resolution
```bash
python3 dummy_analyzer_per_mod_part2.py
```

4) Plot
```bash
cd plotter
python3 Plot_hit_resolution.py 
```

#include "TreeReader.h"
using namespace std;

TreeReader::TreeReader(string fName_){
    _ff = new TFile(fName_.c_str());
    _tt = (TTree *) _ff->Get("anResol/reso");
    n_entries = 0;
    if (_tt == 0)  
        return;
    event_counter = 0;
    n_entries = _tt->GetEntries();
    _tt->SetBranchAddress("detID1"     ,&(event.detID1));
    _tt->SetBranchAddress("detID2"     ,&(event.detID2));
    _tt->SetBranchAddress("pitch1"     ,&(event.pitch1));
    _tt->SetBranchAddress("momentum"   ,&(event.momentum));
    _tt->SetBranchAddress("pairPath"   ,&(event.pairPath));
    _tt->SetBranchAddress("trackChi2"  ,&(event.trackChi2)); //Prob
    _tt->SetBranchAddress("clusterW1"  ,&(event.clusterW1));
    _tt->SetBranchAddress("clusterW2"  ,&(event.clusterW2));
    _tt->SetBranchAddress("numHits"    ,&(event.numHits));
    _tt->SetBranchAddress("hitDX"      ,&(event.hitDX));
    _tt->SetBranchAddress("trackDX"    ,&(event.trackDX));
    _tt->SetBranchAddress("trackDXE"   ,&(event.trackDXE));
    _tt->SetBranchAddress("atEdge1"   ,&(event.atEdge1));
    _tt->SetBranchAddress("atEdge2"   ,&(event.atEdge2));
}

TreeReader::~TreeReader(){
    _ff->Close();
    delete _ff;
}

int TreeReader::get_event_counter(){
    return event_counter;
}

int TreeReader::getEvent(int event_number){
    if (_tt == 0)  
        return 0;
    if (event_number < 0){
        if (event_counter < n_entries){
            _tt->GetEntry(event_counter);
            event_counter++;
            return 1;
        }
        else{
            return 0;
        }
    }
    else{
        if ((uint) event_number < n_entries){
            event_counter = event_number;
            _tt->GetEntry(event_number);
            return 1;
        }
        else{
            return 0;
        }
    }
}


void TreeReader::reset(){
    event_counter = 0;
}

uint64_t TreeReader::getEntries(){
    return n_entries;
}
string   TreeReader::getProgress(){
    return "Processed "+to_string(event_counter) + " / "+ to_string(n_entries)+" : ~" + to_string((100*event_counter)/n_entries)+"%";

}

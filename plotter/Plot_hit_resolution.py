import ROOT as rt
import CMS_lumi, tdrstyle
from array import array

rt.gROOT.SetBatch(1)
#set the tdr style
tdrstyle.setTDRStyle()

#change the CMS_lumi variables (see CMS_lumi.py)
CMS_lumi.writeExtraText = 1
CMS_lumi.extraText = "Preliminary"
# CMS_lumi.lumi_sqrtS = "1.3fb^{-1} (2023, 13.6 TeV)" # used with iPeriod = 0, manual.
CMS_lumi.lumi_sqrtS = "(2023) 13.6 TeV" # used with iPeriod = 0, manual.
iPeriod = 0

#CMS over the pad
iPos = 0
if( iPos==0 ): CMS_lumi.relPosX = 0.16 #Manual tuning of the offset

offset_size = 0.7
H_ref = 800;
W_ref = 800;
W = W_ref
H  = H_ref

# references for T, B, L, R -> Relative borders
T = 0.08*H_ref
B = 0.16*H_ref
L = 0.16*W_ref
R = 0.08*W_ref

canvas = rt.TCanvas("Hit_resolution","Hit_resolution",W_ref,H_ref)
def offset(p,w):
    modifier = 0
    # print(p)
    if p ==  0.012:
        modifier = -2
    elif p == 0.0122:
        modifier = +2
    print(p,w,w-2+modifier)
    return (w-2+modifier)*offset_size


input_file = rt.TFile("../output.root")
pitches = []
for hist in input_file.GetListOfKeys():
    pitches.append(float(hist.GetName().split("_")[1]))
pitches = list(set(pitches))
pitches.sort()
cols =[207, 210, 216]
marker = [20,22,4]
graphs = []
for w in range(1,4):
    # offset = (w-2)*offset_size
    x_axis = [10000*p+offset(p,w) for p in pitches]
    y_axis = []
    dy     = []
    for p in pitches:
        hist = input_file.Get(f"Resolution_{p}_{w}")
        y_axis.append(hist.GetMean())
        dy.append(hist.GetStdDev())
    x_axis.append(50)
    y_axis.append(-1000)
    x_axis.append(200)
    y_axis.append(-1000)
    gg = rt.TGraphErrors(len(x_axis), array('f',x_axis),array('f',y_axis),array('f', [0 for x in x_axis]), array('f', dy    ))
    gg.SetMarkerColor(cols[w-1])
    gg.SetLineColor(cols[w-1])
    gg.SetMarkerStyle(marker[w-1])
    gg.SetMarkerSize(1.5)
    # gg.SetLineColor(cols[w-1])
    if w == 1:
        gg.Draw("AP")
        gg.GetYaxis().SetRangeUser(0,70)
        gg.GetXaxis().SetRangeUser(50,200)
        gg.GetXaxis().SetTitle("Strip pitch (#mum)")
        gg.GetYaxis().SetTitle("Hit resolution (#mum)")
    else:
        gg.Draw("P same")
    graphs.append(gg)

line = rt.TLine(10000*pitches[0],10000*pitches[0]/12**0.5, 10000*pitches[-1], 10000*pitches[-1]/12**0.5)
line.SetLineWidth(2)
line.SetLineStyle(2)
line.Draw("same")
leg = rt.TLegend(0.2,0.65,0.5,0.9)

leg.AddEntry(graphs[0], "Cluster Width = 1", "P")
leg.AddEntry(graphs[1], "Cluster Width = 2", "P")
leg.AddEntry(graphs[2], "Cluster Width = 3", "P")
leg.AddEntry(line, "pitch/#sqrt{12}", "L")







leg.SetBorderSize(0)
leg.Draw()



canvas.SetFillColor(0)
canvas.SetBorderMode(0)
canvas.SetFrameFillStyle(0)
canvas.SetFrameBorderMode(0)
canvas.SetLeftMargin( L/W )
canvas.SetRightMargin( R/W )
canvas.SetTopMargin( T/H )
canvas.SetBottomMargin( B/H )
canvas.SetTickx(0)
canvas.SetTicky(0)

canvas.SetLogz()

canvas.Draw()


CMS_lumi.CMS_lumi(canvas, iPeriod, iPos)
canvas.SetWindowSize(W_ref,H_ref)
canvas.cd()
canvas.Update()
canvas.Print("Hit_Resolution.pdf")
canvas.Print("Hit_Resolution.png")
canvas.Print("Hit_Resolution.root")
canvas.Print("Hit_Resolution.C")



from ROOT import TFile, TH1F,gROOT
from math import sqrt
import argparse
gROOT.SetBatch(1)

pitches = { 'TIB Layer 1' : 0.008 ,
            'TIB Layer 2' : 0.008 ,
            'TIB Layer 3' : 0.012 ,
            'TIB Layer 4' : 0.012 ,
            'TOB Layer 1' : 0.0183, 
            'TOB Layer 2' : 0.0183, 
            'TOB Layer 3' : 0.0183, 
            'TOB Layer 4' : 0.0183, 
            'TOB Layer 5' : 0.0122, 
            'TOB Layer 6' : 0.0122} 


def get_name(detId):
    return ["", "", "", "TIB", "TID", "TOB", "TEC"][get_subdet(detId)]+f" Layer {get_layer(detId)}"


def get_subdet(detId):
    return ((detId >> 25) & 0x7)


def get_layer(detId):
    return (detId >> 14) & 0x7



fo = TFile('histograms_per_size.root')
meas_list = []
for hist in fo.GetListOfKeys():
    name = hist.GetName()
    if "Double_difference_" in name:
        meas_list.append(name.replace("Double_difference_",""))


hist_res_vs_pitch = {}
for meas_name in meas_list:

    dd = fo.Get("Double_difference_"+str(meas_name))
    te = fo.Get("Track_error_"+str(meas_name))
    
    detId = int(meas_name.split("_")[0])
    width = int(meas_name.split("_")[1].replace("w",""))

    if dd.GetEntries() > 10:
        dd.Fit("gaus", "lq")
        dde = dd.GetFunction("gaus").GetParameter(2)
        tte = te.GetMean()
        subDet = get_name(detId)
        pitch = pitches[subDet]

        name_pitch = str(pitch)+"_"+str(width)
        if name_pitch not in hist_res_vs_pitch.keys():
            hist_res_vs_pitch[name_pitch] = TH1F("Resolution_"+name_pitch, "Resolution_"+name_pitch, 25, 0, 0.01*10000)

        # if subDet not in hist_res.keys():
        #     hist_res[subDet] = TH1F("Resolution"+subDet, "Resolution"+subDet, 25, 0, 0.01*10000)
        if tte < dde:
            # hist_res[subDet].Fill(10000*sqrt(dde**2-tte**2)/sqrt(2))
            hist_res_vs_pitch[name_pitch].Fill(10000*sqrt(dde**2-tte**2)/sqrt(2))

output_file = TFile("output.root","RECREATE")
# for hh in hist_res:
#     print(hh)
#     hist_res[hh].Fit("gaus", "l")
#     hist_res[hh].Write()

for hh in hist_res_vs_pitch:
    hist_res_vs_pitch[hh].Write()
output_file.Close()


_DEPS = TreeReader.h HistManager.h HelperFunctions.h
_OBJ = TreeReader.o HistManager.o

IDIR = ./include
LDIR = ./lib
ODIR = ./src
PDIR = ./plugins
BDIR = ./bin


PLUGINS = $(patsubst %.cc,%.out,$(patsubst plugins/%,$(BDIR)/%,$(wildcard plugins/*.cc)))
DICTC = Dict.cc
DICTH = Dict.h

CFLAGS = -I$(IDIR) -I./ 
DEPS = $(patsubst %,$(IDIR)/%,$(_DEPS))
OBJ  = $(patsubst %,$(ODIR)/%,$(_OBJ))
CC = g++
COpt = -Wall -Wextra  `root-config --cflags --libs` -O3



all:
	@echo "############# COMPILING ALL #############"
	@echo "###### COMPILING DEPENDENCIES..."
	@make  -j4 $(OBJ)
	@echo "###### COMPILING $(PLUGINS)..."
	@make -j8 $(PLUGINS)

%.o: %.cc
	$(CC) -c -o $@ $< $(CFLAGS) $(ROOTLIBS) $(COpt) 


bin/%.out: plugins/%.cc $(OBJ)
	$(CC)  -o $@ $^ $(CFLAGS) $(ROOTLIBS) $(COpt)


.PHONY: clean

clean:
	rm -rf $(ODIR)/*.o hellomake src/Dict.cc include/Dict.h bin/* plugins/*.out

  
